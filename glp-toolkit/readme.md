GLP Toolkit for Node.js
=======================

Copyright (c) 2017 Echelon Corporation

Specification
-------------

The toolkit specification is available with restricted access on
https://echeloncorp-apollo.pbworks.com/w/page/112407376/_index

Installation
------------

Place the _glp_ folder and all its contents into a suitable location along the
Node.js module search path. /node_modules is recommended.
