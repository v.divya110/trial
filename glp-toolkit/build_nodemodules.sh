#!/bin/bash

## This is a simple script to do `npm install` wherever required. This script
## is supposed to be run from the root of the repository, only after a fresh 
## `git clone` or `git checkout` or similar.

# New directory names to be appended to this list, with space as separator.
DIR_LIST="glp glp/config glp/log glp/mqtt glp/object"

CWD=$PWD
for dir in ${DIR_LIST} ; do
	cd ${dir}
	rm -rf node_modules
	echo "--- Building node_modules in: ${PWD}"
	echo
	npm install
	if [ $? -ne 0 ] ; then
		echo -e "\e[31m\e[7WARNING: Failed to build node_modules for ${dir}"
	echo
	fi
	cd ${CWD}	
done

