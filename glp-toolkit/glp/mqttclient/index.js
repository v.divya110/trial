/*
 * Echelon GLP Toolkit for Node.js
 * Copyright (c) 2017 Echelon Corporation
 * MQTT module: provides a resilient client which automatically re-connects
 * when the connection drops, and re-subscribes when a connection is
 * (re-) established. Runtime errors, such as a dropped and re-established
 * connection to the broker, are reported through the GLP Generic Logger
 * Service and the syslog.
 * The module also provides a dispatcher service for incoming messages and
 * useful  utilities and monitors and preserves the segment identifier
 */
 /* jshint esversion: 6 */
 /* jslint node: true, maxerr: 10000 */
"use strict";
const tools = require("../tools").v0;
let self = require("./package.json");
const version = tools.get_version(self.version);
const format_template = require("string-template");
const lit = require("../spec/").v0;
const log = require("../log").v0;
const mqtt = require("mqtt");
const mqtt_regex = require("mqtt-regex");
const sqlite3 = require("sqlite3").verbose();
const PERIOD = ".";
const SQLITE_PCRE_EXTENSION = "/usr/lib/sqlite3/pcre.so";
const ADMIN_TABLE = "GLPAdminTable";
const EventEmitter = require("events").EventEmitter;

// mqtt object which contains information required for connection and topics
let mqtt_cfg = {};
let macro_map = {};
// literals defined in config
const config = require('../config').v0;

// default topics to subscribe
const SID_TOPIC = 'glp/0/././sid';

// global sid object
let global_sid = null;
let glp_db_path;
let sid = '';

// event emitter 
let sid_value = new EventEmitter();

const CHANNEL = {
  rq : lit.REQUEST_CHANNEL,
  fb : lit.FEEDBACK_CHANNEL,
  ev : lit.EVENT_CHANNEL
};

/*
 * populate_macro_map
 * This function will populate the map when the sid value changes
 */

function populate_macro_map(cur_sid) {
    const GLP   = "glp/";
    const MACID = mqtt_cfg.getter("." + config.MACID);
    const LOG   = "log/event";
    macro_map = {
        0   : GLP + "0/" + cur_sid,
        1   : GLP + "1/" + cur_sid,
        2   : GLP + "2/" + cur_sid,
        3   : GLP + "3/" + cur_sid,
        P0  : GLP + "0/" + ":" + MACID,
        P1  : GLP + "1/" + ":" + MACID,
        P2  : GLP + "2/" + ":" + MACID,
        P3  : GLP + "3/" + ":" + MACID,
        L0  : GLP + "0/./" + LOG,
        MAC : ":" + MACID,
        SID : cur_sid
    };
 }

/*
 * populate_mqtt_obj
 * This function will copy mqtt related data from config object
 * This function does not return anything.
 */
function populate_mqtt_obj(getter) {

   //Need to add period in the started for standard configuration options
   const opt = [config.MQTT_PORT, config.MQTT_BROKER, config.MQTT_USERNAME,
          config.MQTT_PASSWORD, config.MQTT_PROTOCOL, config.MQTT_VERSION,
          config.MQTT_CLIENTID, config.GLP_DB_PATH];
   for (let entry of opt) {
       mqtt_cfg[entry] = getter(PERIOD + entry);
   }
   mqtt_cfg[config.TOPICS]   = {};
}

/*
 */
function read_sid_information(dbpath, callback) {
    let db = new sqlite3.Database(dbpath, sqlite3.OPEN_CREATE |
            sqlite3.OPEN_READWRITE, function(error) {
        if(error) {
            callback(error);
         } else {
            db.loadExtension(mqtt_cfg.getter(PERIOD + config.SQL_PCRE_PATH));
            callback(null, db);
         }
    });
    db.on('error', function(error) {
        log.fatal(error);
    });
}


/*
 * get_sid_from_db()
 */
function get_sid_from_db(err, db) {
    let cmd = "select * from " + lit.ADMIN_TABLE + " where name = 'sid'";
    db.all(cmd, function(err, row) {
        if (!err) {
            if (row.length > 0) {
                for (let entry of row){
                    if (entry.name == "sid") {
                        global_sid.current_value = entry.value;
                    }
                }
            } else {
                global_sid.current_value = '';
                update_sid_in_admin_table(global_sid.current_value, false);
            }
            sid_value.emit('sid');
            populate_macro_map(global_sid.current_value);
        } else {
            log.mqtt(err);
        }
        db.close();
    });
}



/*
 * check if GLPAdmin table exists of not
 */
function read_glp_admin_tables(err, db) {
    let query;
    let params = "";

    const create_table_cmd = "CREATE TABLE if not exists ";
    params = "(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, value TEXT)";
    query = create_table_cmd + lit.ADMIN_TABLE + params;
    db.run(query, function(err) {
        if(err) {
            log.mqtt(err);
        } else {
            get_sid_from_db(null, db);
        }
    });
}


/*
 * init
 * initializes the module for a client with the given name, yields Error or
 * undefined for success.
 * The function accepts a getter with the signature of config.v0.get.
 */
function init(name, getter) {

    let error, err_msg;
    
	mqtt_cfg.getter = getter;
	mqtt_cfg.name   = name;
    // Check if getter is supplied and not null
    if (!getter) {
        err_msg = "mqtt.init(): the 'getter' is missing";
        error = new Error(err_msg);
    }

	populate_mqtt_obj(mqtt_cfg.getter);
    populate_macro_map(global_sid);


    if (!error) {
        global_sid = getsid();
        read_sid_information(getter(PERIOD + config.GLP_DB_PATH),
                             read_glp_admin_tables);
    }
    return error;
}

/*
 * connect
 * connects to the MQTT broker, yields Error in case of failure or undefined in
 * case of success.
 * The function re-connects when the connection drops and re-subscribes all
 * previously subscribed topics whenever a connection is established.
 */
function connect(connectEventsHandler) {

    let conn_options = {};
    let mqtt_client, handler;

	populate_mqtt_obj(mqtt_cfg.getter);

    // Create MQTT clientId
    mqtt_cfg[config.MQTT_CLIENTID] = mqtt_cfg.name + PERIOD +
                                     mqtt_cfg[config.MQTT_CLIENTID];

    // Creates an MQTT URL for connection
    const url = mqtt_cfg[config.MQTT_PROTOCOL] + "://" +
                mqtt_cfg[config.MQTT_BROKER] + ":" +
                mqtt_cfg[config.MQTT_PORT];

    // Sets mqtt options for connection
    // Connection option indices cannot be replaced by literals since
    // the indices are keywords
    conn_options.clientId = mqtt_cfg[config.MQTT_CLIENTID];
    conn_options.username = mqtt_cfg[config.MQTT_USERNAME];
    conn_options.password = mqtt_cfg[config.MQTT_PASSWORD];
    conn_options.protocol = mqtt_cfg[config.MQTT_PROTOCOL];
    conn_options.protocolVersion = Number(mqtt_cfg[config.MQTT_VERSION]);
    try {
        mqtt_client = mqtt.connect(url, conn_options);
    } catch(e) {
        log.mqtt(e);
    }

    /*
     * Event 'error': Emitted when there is an error in connection with
     * the broker.
     * This event returns a callback with the signature
     * callback(err, msg) where err is the error object if an error occurs and
     * msg is a string value which states "Error in connection" in case of
     * error.
     * It yields error diagnostics which are reported through log.
     */
    mqtt_client.on("error", function(error){
        connectEventsHandler(error, error.message);
        log.error(error.message);
    });

    /*
     * Event 'offline': Emitted when the broker is unavailable or if the
     * client tries to connect with a duplicate client-id i.e. the client-id
     * is already in use by another client.
     * This event yields MQTT diagnostics which are reported through log.
     */
    mqtt_client.on("offline", function(error){
        log.warning("Mqtt broker is offline");
    });


    /*
     * Event 'reconnect': Emitted when the client tries to reconnect to the
     * broker in case of connection drops.
     * This event yields warning diagnostics which are reported through log.
     */
    mqtt_client.on("reconnect", function() {
        log.info("Reconnecting to mqtt broker");
    });

    /*
     * Event 'connect': Emitted when the client tries to connect to the broker.
     * Username and password verification and validation is done by this
     * event before it connects to the MQTT broker successfully.
     * This event sends a callback after the client is successfully connected
     * with the following signature callback(err,msg) where err is an error
     * object in case of error and msg is a string.
     */
    mqtt_client.on("connect", function(connack) {
        mqtt_cfg[config.MQTT_CLIENT] = mqtt_client;
        //register topic for sid change
        //global_sid = getsid();
        register(SID_TOPIC, process_sid_message, true);
        log.info("Connack ", "ReturnCode: " , connack.returnCode);
        connectEventsHandler(null, "Connected to mqtt broker");
    });

    /*
     * Event 'message': Emitted when a message arrives for a subscribed topic.
     * In this function, the topic is matched with the registered topics in
     * mqtt_cfg object, when a match is found, the message is dispatched to the
     * registered handler.
     */
    mqtt_client.on("message", (topic, message) => {
      // Searches for the handler registered for the topic
        for (let each in mqtt_cfg[config.TOPICS]) {
          // If there is a match with the topic received and the topic subscribed,
          // the topic's corresponding handler is assigned from the config object
          // The handler function is then called with the signature
          // handler(err, subscription, topic, message) where err is null
          // because no error occurred
            if (match(each, topic)) {
                handler = mqtt_cfg[config.TOPICS][each];
                if (handler !== undefined) {
                    handler(null, each, topic, message);
                } else {
                    // no handler found calling default handler
                    handler = mqtt_cfg[config.TOPICS][null];
                    if(handler){
                        let info_msg = "Calling default handler, as no handler " +
                                       "for topic:" + topic;
                        log.info(info_msg);
                        handler(null, each, topic, message);
                    } else {
                          let err_msg = "No default handler found";
                          log.error(err_msg);
                    }
                }
            }
        }
    });
}

/*
 * disconnect
 * disconnects client from the MQTT broker. The function always succeeds,
 * but it may disconnect gracefully or without grace.
 * Disconnecting ungracefully may yield error or warning diagnostics,
 * reported through log, but will not yield an Error result of this API.
 * This function unsubscribes if it is passed as a parameter otherwise the
 * client disconnects the connection gracefully, yields info diagnostics,
 * reported through log.
 */
function disconnect(unsubscribe) {

    for(let each in mqtt_cfg[config.TOPICS]) {
        deregister(each, unsubscribe);
    }
    // disconnect mqtt client
    log.info("Disconnecting mqtt connection");
    mqtt_cfg[config.MQTT_CLIENT].end();
}


/*
 * resolve_mqtt_topic
 * This function inspect mqtt topics, looks for macros
 * if any, it replaces them and generate valid mqtt topic
 * For more information refer
 * https://echeloncorp-apollo.pbworks.com/w/page/112417798/mqtt
 * in case of error, it returns null
 */

function resolve_mqtt_topic(topic) {
    if(topic) {
      let resolved_topic;
      resolved_topic = format_template(topic, macro_map);
      return resolved_topic;
    }
}

/*
 * publish
 * publishes message to topic, using retain and qos preferences. The
 * function calls a completion handler with this signature, if one was
 * provided: completion(err, topic, message) where topic, message match
 * the arguments to this function, err is an Error object if an error
 * occurred.
 */
function publish(topic, message, retain, qos, completion) {

    let options = {};
    let resolved_topic;
    let error, err_msg;

    options.qos = qos;
    options.retain = retain;
    if (!completion) {
        err_msg = "completion callback can not be null";
        error = new Error(err_msg);
        log.mqtt(error);
    }

    if(!error)
    {
        // checks if mqtt client is connected, yields error in case of failure
        if (mqtt_cfg[config.MQTT_CLIENT] !== undefined) {
            // checks if the connected option in the mqtt_client is true
            if (mqtt_cfg[config.MQTT_CLIENT].connected) {
                let parsed_topic  = resolve_mqtt_topic(topic);
                if (parsed_topic instanceof Error) {
                    error = parsed_topic;
                } else {
                    mqtt_cfg[config.MQTT_CLIENT].
                        publish(parsed_topic, message, options, function(error){
                            completion(error, parsed_topic, message);
                            if (error instanceof Error) {
                                log.mqtt(error);
                            }
                        });
                }
            } else {
                err_msg = "MQTT client is not connected";
                log.mqtt(err_msg);
            }
        } else {
            err_msg = "MQTT client is undefined";
            log.mqtt(new Error(err_msg));
        }
    }
}

/*
 * register
 * subscribes to an MQTT topic (MQTT wildcards may be used), then invokes the
 * handler when a message on a matching topic is received. The handler has
 * this signature: handler(error, subscription, topic, message), where topic,
 * message are the topic and message actually received. Subscription is the
 * topic specified with the registration, and error is an Error object if an
 * error occurred.
 */
function register(topic, handler, subscribe) {

    let parsed_topic  = resolve_mqtt_topic(topic);
    let error, err_msg;
    if( !handler) {
        err_msg = "Handler can not be null";
        error = new Error(err_msg);
    }
    if(!error) {
        if(mqtt_cfg[config.MQTT_CLIENT]) {
            if (mqtt_cfg[config.MQTT_CLIENT].connected) {
                log.debug("Registering topic ", parsed_topic);
                mqtt_cfg[config.TOPICS][parsed_topic] = handler;
                if(subscribe)
                    mqtt_cfg[config.MQTT_CLIENT].subscribe(parsed_topic);
              } else {
                    log.mqtt("MQTT client is not connected");
              }
        } else {
            err_msg = "MQTT client is undefined.";
            error = new Error(err_msg);
        }
    }
    return error;
}

/*
 * deregister
 * removes a subscription and the associated  handler. This function always
 * succeeds even if there is no subscription
 */
function deregister(subscription, unsubscribe) {
    let error, err_msg;
    log.debug("Deregistering topic ", subscription);
    if (unsubscribe) {
        if(mqtt_cfg[config.MQTT_CLIENT]) {
            if(mqtt_cfg[config.MQTT_CLIENT].connected) {
                mqtt_cfg[config.MQTT_CLIENT].unsubscribe(subscription);
            } else {
                  log.mqtt("MQTT client is not connected");
            }
        } else {
              err_msg = "MQTT client is undefined.";
              error = new Error(err_msg);
        }
    }
    delete mqtt_cfg[config.TOPICS][subscription];

    return error;
}

/*
 * extract
 * takes a matching subscription, topic pair and returns an ordered list of
 * topic values for the wildcards in the subscription. For example,
 * extract('glp/0/+/rq/job/+/#', 'glp/0/seg.23/rq/job/1234/do')
 * yields [ 'seg.23', '1234', 'do' ].
 * The function always returns an array of strings, which might be empty if
 * no extractions are found.
 */
function extract(subscription, topic) {

    let cases = [topic];
    let params = [];
    let compiled = mqtt_regex(subscription);
    cases.forEach(function(topic, index) {
        let matches = compiled.regex.exec(topic);
        // matches is an array which contains the input string, values for the
        // wildcards in the subscription and the index
        // index:0 has the input string followed by the wildcard values followed
        // by the index value in the end of the array
		    if (matches) {
            for (let i = 1; i <= matches.length - 1; i++) {
                if(matches[i].indexOf("/") > -1) {
                    let elements = matches[i].split("/");
                    for (let each of elements) {
                        if (each !== "") {
                            params.push(each);
                        }
                    }
                } else {
                    params.push(matches[i]);
                }
			}
	      }
	  });

    return params;
}

/*
 * match
 * returns a boolean to indicate whether the topic matches the subscription.
 * For example, match('glp/0/#', 'glp/0/test') yields true, but
 * match('glp/1/#', 'glp/0/test') yields false.
 */
function match(sub, topic) {

    let cases = [topic];
    let flag = true;
    let compiled = mqtt_regex(sub);
    let matches;

    cases.forEach(function(topic, index) {
        matches = compiled.regex.exec(topic);
        if(matches) {
            flag = true;
        } else {
            flag = false;
        }
    });

    return flag;
}

/*
 * transform
 * The transform function transforms one topic into an other: if ch is not null,
 * the function changes the given topic's GLP channel to the value of ch.
 * If tail is not null, the function replaces the given topic's tail end with
 * n elements with the value of tail:
 * v0.transform(topic, ch, tail, n) => string
 * For example, transform('glp/0/seg.23/rq/dev/lt/3/do',
                          glp.v0.spec.FEEDBACK_CHANNEL,
                          glp.v0.spec.CONFIG_OBJECT, 1)
 * yields 'glp/0/seg.23/fb/dev/lt/3/cfg'.
 */
function transform(topic, ch, tail, n) {
    let pattern, re, replace;
    let error, err_msg;

    // if ch is not null
    if(ch) {
        // search for channel in the topic
        for(let channel in CHANNEL) {
            pattern = "\/(" + channel + ")\/";
            re = new RegExp(pattern);
            topic = topic.replace(re, "/" + ch + "/");
            break;
        }
    }

    // if tail is not null
    if(tail) {
        // Remove tail from the topic and replacing it with a new tail
        let words = topic.split("/");
        if (n > words.length) {
            err_msg = "Number of elements to be replaced in tail can not" +
                      " be greater than the number of elements in the topic";
            error = new Error(err_msg);
        }
        if (!error) {
            words = words.slice(0, words.length - n );
            words[words.length] = tail;
            topic = words.join("/");
        }
    }
    if (!error) {
        return topic;
    }
    return error;
}

/*
/*
 * update_sid_in_admin_table()
 */
function update_sid_in_admin_table(sid, update) {
    let db = new sqlite3.Database(mqtt_cfg[config.GLP_DB_PATH],
                                  sqlite3.OPEN_CREATE |sqlite3.OPEN_READWRITE,
             function(error) {
                if(error) {
                    log.fatal(error);
                } else {
                    let query = "";
                    db.loadExtension(config.get(PERIOD + config.SQL_PCRE_PATH));
                    if(!update) {
                        query = "INSERT INTO " + ADMIN_TABLE +
                                "(id, name, value) VALUES(null,'sid','')";
                    } else {
                          query = "UPDATE " + ADMIN_TABLE + " set value = \'" +
                                  sid + "\' where name = \'sid\'";
           }
           db.run(query);
           db.close();
        }
   });

   db.on('error', function(error){
      log.fatal(error);
   });

}

/*
 * process_sid_message: This function is an SID handler for global_sid.
 */
function process_sid_message(error, subscription, topic, msg) {
    let sid, err;

    if(!error) {
        try{
            sid = JSON.parse(msg.toString());
        } catch(e) {
            error = e;
        }

        if(typeof(sid) !== typeof("")) {
            let err_msg = "Invalid type for sid";
            error = new Error(err_msg);
        }

        if(!error) {
            if (global_sid.current_value != sid) {
                update_sid_in_admin_table(sid, true);
                populate_macro_map(sid);
                for (let handle of global_sid.sid_handlers) {
                    handle(global_sid.current_value, sid);
                }
                global_sid.current_value = sid;
            }
        }
    }
    if(error) {
        log.error(error);
    }
}

/*
 * SID class
 */

class SID {
      constructor(current_value, sid_handlers) {
          this.current_value = null;
          this.sid_handlers = [];
      }


      /*
       * value()
       * a simple getter for the current SID value. The function returns a valid
       * SID (a string of length 1 or more), or null.
       */
       value() {
           sid_value.on('sid', function() {
               return this.current_value;
           });
       }

      /*
       * register()
       * registers one or more event handlers for notification on SID changes.
       * The handlers are called when the new SID has been accepted, and must
       * support this signature: handler(previous_sid, current_sid)
       */
       register(handler) {
          let error;
          if(handler){
              log.debug("Registering sid handler ", handler);
              this.sid_handlers.push(handler);
          } else {
              error = new Error("SID handler is not provided");
          }
          return error;
       }

      /*
       * deregister
       * removes a previously registered handler
       */
       deregister(handler) {
          log.debug("Deregistering sid handler ", handler);
          let index = this.sid_handlers.indexOf(handler);
          if (index != -1) {
              this.sid_handlers.splice( index, 1 );
          }
       }
}

/*
 * getsid
 * Function which returns an object of type SID. This is to make sure that we have
 * singleton approach
 */
function getsid() {
    if (global_sid === null) {
        global_sid = new SID();
    }
    return global_sid;
}

exports.v0 = {
    VERSION: version,
    init: init,
    connect: connect,
    disconnect: disconnect,
    publish: publish,
    register: register,
    deregister: deregister,
    extract: extract,
    match: match,
    transform: transform,
    resolve_mqtt_topic: resolve_mqtt_topic,
    SID: getsid
};
