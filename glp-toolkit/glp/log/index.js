/*
 * Echelon GLP Toolkit for Node.js
 * Copyright (c) 2017 Echelon Corporation
 * This module is used for logging errors, warnings, debug
 * from other modules
 */

/* jshint esversion: 6 */
/* jslint node: true, maxerr: 10000 */

"use strict";
const tools = require("../tools").v0;
let self = require("./package.json");
const version = tools.get_version(self.version);

const syslog    = require('modern-syslog');
const moment    = require('moment-timezone');
const columnify = require('columnify');
const util      = require('util');

//const CONFIG = require('../config').v0.config_lit;
const config = require('../config').v0;
const LOG_EVENT_TOPIC = "glp/0/./log/event";
const PERIOD          = ".";
const LOG_LANGUAGE    = "en";

const LOG_SEVERITY_DEBUG   = "debug";
const LOG_SEVERITY_INFO    = "info";
const LOG_SEVERITY_WARNING = "warning";
const LOG_SEVERITY_MQTT    = "mqtt";
const LOG_SEVERITY_ERROR   = "error";
const LOG_SEVERITY_FATAL   = "fatal";

/* Maps a severity level to a Linux syslog log level */
const LOG_SYSLOG_MAP = {

    LOG_SEVERITY_DEBUG   : syslog.level.LOG_DEBUG,
    LOG_SEVERITY_INFO    : syslog.level.LOG_INFO,
    LOG_SEVERITY_WARNING : syslog.level.LOG_WARNING,
    LOG_SEVERITY_MQTT    : syslog.level.LOG_ERROR,
    LOG_SEVERITY_ERROR   : syslog.level.LOG_ERROR,
    LOG_SEVERITY_FATAL   : syslog.level.LOG_ALERT

};

/* Used for printing columnar output */
const LOG_COL_MIN_WIDTH = 20;
const LOG_COL_MAX_WIDTH = 30;

/*
 * Variables for use within this module, initialized by init()
 */
let debug_mode = false;
let verbosity = 0;
let publisher = null;
let service_name, get;

/*
 * init
 * initializes the module for a client with the given name, yields Error or
 * undefined for success.
 * initializes the publish function and syslog
 * The function accepts a getter of config.v0.get, and a
 * publisher of mqtt.v0.publish
 */
function init(name, getter, publish) {
    // We assume that name, getter and publish are valid. The onus of
    // correctness is upon the caller, in this case.
    debug_mode   = getter(PERIOD + config.LOG_DEBUG) === "1";
    publisher    = publish;
    service_name = name;
    get = getter;
    return;
}

/*
 * generate_event_object:
 * This function returns a GLP event object to be published on the topic
 * glp/0/./log/event where the Generic Logger Service is subscribed.
 */
function generate_event_object(severity, message) {

    const FORMAT = "YYYY-MM-DD HH:mm:ss.SSS ";
    const urgent = severity === LOG_SEVERITY_FATAL;

    const date   = new Date();
    const time   = date.getTime();
    const mmt    = moment();
    const tz     = moment.tz.guess();
    const abbr   = mmt.tz(tz).zoneAbbr();
    const ts     = moment(date).format(FORMAT) + abbr;
    const ts_utc = moment(date).utc().format(FORMAT) + "UTC";
    const topic = "{0}/" + "ev/" + severity;
    const EVENT_OBJ = {
        id      : time,
        cat     : severity,
        utc     : ts_utc,
        language: LOG_LANGUAGE,
        local   : ts,
        message : message,
        topic   : topic,
        urgent  : urgent
    };
    return EVENT_OBJ;
}

/*
 * diagnostics
 * An internal variadic utility used to implement the debug, info, warning,
 * error, mqtt and fatal functions.
 * severity indicates the severity of the log message.
 * publish is a boolean to control whether an attempt is made to publish the
 * diagnostic to the MQTT message broker.
 * print is a boolean to control whether the diagnostics is shown on the console.
 * do_sys_log is a boolean to control whether the diagnostics is to be logged in the
 * system
 * fatal() and debug() show Error object's stacktrace, if  available.
 */

function diagnostics(severity, publish, print, do_sys_log, e, ...args) {

    if(!verbosity){
      verbosity    = get(PERIOD + config.LOG_VERBOSITY);
    }

    if (publish || print || do_sys_log) {
        let err_stack = "";
        let err_message = "";
        const timestamp = moment(new Date()).format('H:mm:ss.SSS ZZ');

        if (e instanceof Error) {
            err_message = e.message;
            err_stack = e.stack;
        } else {
            err_message = format(e, ...args);
            err_message = err_message + "\n";
        }

        // mqtt related errors should not be published via mqtt
        if (publish && publisher) {

            const glp_event_obj = generate_event_object(severity, err_message);
            const topic = "{L0}";
            publisher(topic, JSON.stringify(glp_event_obj), false, 0,
                      function(error, topic, msg) {
                        if (error && severity != LOG_SEVERITY_FATAL) {
                          mqtt(error); //this also prints to the console.
                        }
                      });
        }

        if (print) {
            // columnify is used to display logs in a tabular fashion with
            // headings as the key names
            // Note: The headings are derived from variable names even if keys are not specified
            let columns = columnify([{
                "Timestamp"    : timestamp,
                "Severity"     : severity,
                "Service Name" : service_name,
                "Message"      : err_message

            }], {
                minWidth: LOG_COL_MIN_WIDTH,
                config: {
                    description: {
                        maxWidth: LOG_COL_MAX_WIDTH
                    }
                }
            });

            console.log(columns);
            if (severity === LOG_SEVERITY_FATAL || severity === LOG_SEVERITY_DEBUG) {
                if (err_stack.length > 0) {
                    console.error("\n" + err_stack + "\n");
                }
            }
        }

        if (do_sys_log) {

            syslog.log(LOG_SYSLOG_MAP.severity,
                       timestamp + " " +
                       service_name + " " +
                       err_message + "\n" +
                       err_stack,
                       function() {});
        }

    } // we will never hit the 'else' case, hence skipping it.
}

/*
 * Format function is used by the fatal, error, mqtt, warning, info and debug
 * API to construct the diagnostic message, but toolkit clients may also use
 * the same format() API directly.
 * The function accepts an argument e. If e is an instanceof Error, the resulting
 * string is the error object and all other arguments are ignored.
 * If e is not an instance of Error, e and all other
 * arguments to this function are concatenated to form the result string.
 * Arguments must be strings or convertible to strings.
 * format() does not insert space or other characters between the arguments.
 * and does not append a newline character
 */

function format(e, ...args) {
    let err_str = e;

    if (!(e instanceof Error)) {
        for (let each of args) {
            err_str += each;
        }
    }
    return err_str;
}

/*
 * debug
 * See diagnostics().
 */
function debug(e, ...args) {

    diagnostics(
        LOG_SEVERITY_DEBUG,
        debug_mode,
        verbosity > 3,
        false,
        e,
        ...args
    );
}

/*
 * info
 * See diagnostics().
 */
function info(e, ...args) {
    diagnostics(
        LOG_SEVERITY_INFO,
        true,
        verbosity > 2,
        false,
        e,
        ...args
    );
}

/*
 * warning
 * See diagnostics().
 */
function warning(e, ...args) {
    diagnostics(
        LOG_SEVERITY_WARNING,
        true,
        verbosity > 1,
        false,
        e,
        ...args
    );
}

/*
 * error
 * See diagnostics().
 */
function error(e, ...args) {
    diagnostics(
        LOG_SEVERITY_ERROR,
        true,
        verbosity > 0,
        false,
        e,
        ...args
    );
}

/*
 * mqtt
 * See diagnostics().
 * Use this for errors in relation to MQTT connectivity.
 */
function mqtt(e, ...args) {
    diagnostics(
        LOG_SEVERITY_MQTT,
        false,
        true,
        true,
        e,
        ...args
    );
}

/*
 * fatal
 * See diagnostics().
 * Note this function terminates the process.
 */
function fatal(e, ...args) {
    diagnostics(
        LOG_SEVERITY_FATAL,
        true,
        true,
        true,
        e,
        ...args
    );
    process.exit(-1);
}

exports.v0 = {
    VERSION: version,
    init: init,
    debug: debug,
    info: info,
    warning: warning,
    error: error,
    mqtt: mqtt,
    fatal: fatal
};
