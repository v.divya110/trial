/*
 * GLP Toolkit for Node.js, specs module
 * Copyright (c) 2017 Echelon Corporation
 */
 /* jshint esversion: 6 */
 /* jslint node: true, maxerr: 10000 */
"use strict";
const tools = require("../tools").v0;
let self = require("./package.json");
const version = tools.get_version(self.version);

/*
 * 'lit', short for 'literals', is the export v0 object. We construct this
 * object in a multi-step process by adding sime primitives and literals first,
 * then adding more complex definitions which may use these primitives.
 * The entire object is eventually exported as the v0 API.
 */
let lit = {
    GLP: 'glp',

    GLP_PROTOCOL_MIN_VERSION: 0,
    GLP_PROTOCOL_MAX_VERSION: 0,
    GLP_PROTOCOL_VERSION: 0,

    SEP: '/',
    ANY: '.+?',     // greedy variant: '[^/]+',
    DEPTH: '(?P<DEPTH>(/.+)$',

    REQUEST_CHANNEL: 'rq',
    FEEDBACK_CHANNEL: 'fb',
    EVENT_CHANNEL: 'ev',

    ACTION_ACTIVATE: 'activate',
    ACTION_CREATE: 'create',
    ACTION_DELETE: 'delete',
    ACTION_DEPROVISION: 'deprovision',
    ACTION_LOAD: 'load',
    ACTION_PROVISION: 'provision',
    ACTION_REPAIR: 'repair',
    ACTION_REPLACE: 'replace',
    ACTION_TEST: 'test',
    ACTION_UPDATE: 'update',
    ACTION_WINK: 'wink',

    CAPABILITY_ARBITRARY_ALARM: 'arbitrary-alarms',
    CAPABILITY_BRIDGED_CONNECTION: 'bridged-connections',
    CAPABILITY_DEVICE_GROUPING: 'device-grouping',
    CAPABILITY_SCHEDULER: 'scheduler',
    CAPABILITY_HISTORY: 'history',
    CAPABILITY_PAGINATION_SNAPSHOTS: 'pagination-snapshots',

    CONFIG_OBJECT: 'cfg',

    DATAPOINT_DIRECTION_IN: 'in',
    DATAPOINT_DIRECTION_OUT: 'out',

    DATAPOINT_MONITOR_NONE: 'none',
    DATAPOINT_MONITOR_SLOW: 'slow',
    DATAPOINT_MONITOR_NORMAL: 'normal',
    DATAPOINT_MONITOR_FAST: 'fast',

    DEVICE_CATEGORY_IO: 'i/o',
    DEVICE_CATEGORY_INFRASTRUCTURE: 'infrastructure',
    DEVICE_CATEGORY_SEGMENT: 'segment',

    DEVICE_HEALTH_NORMAL: 'normal',
    DEVICE_HEALTH_SUSPECT: 'suspect',
    DEVICE_HEALTH_DOWN: 'down',
    DEVICE_HEALTH_NASCENT: 'nascent',

    DISCOVERY_METHOD_AUTO: 'auto',
    DISCOVERY_METHOD_NONE: 'none',
    DISCOVERY_METHOD_SATM: 'satm',

    OBJECT_TYPE_DEVICE: 'dev',
    OBJECT_TYPE_GROUP: 'grp',
    OBJECT_TYPE_JOB: 'job',
    OBJECT_TYPE_SCHEDULE: 'sch',
    OBJECT_TYPE_SCHEDULED_EVENT: 'evnt',
    OBJECT_TYPE_CONNECTION: 'con',
    OBJECT_TYPE_ALARM: 'alarm',

    PRIORITY_MIN: 1,            // numerical value minimum
    PRIORITY_MAX: 16,           // numerical value maximum
    PRIORITY_LOWEST:    16,
    PRIORITY_HIGHEST:   1,
    PRIORITY_DEFAULT:   8,

    PROTOCOLS_CONTROLM: 'cm',
    PROTOCOLS_LONTALK: 'lt',
    PROTOCOLS_BLUETOOTH: 'bt',
    PROTOCOLS_MBUS: 'mb',

    STATE_ACTIVE: 'active',
    STATE_CREATED: 'created',
    STATE_DELETED: 'deleted',
    STATE_DISABLED: 'disabled',
    STATE_ENABLED: 'enabled',
    STATE_INACTIVE: 'inactive',
    STATE_PROVISIONED: 'provisioned',
    STATE_UPDATED: 'updated',
    STATE_UNPROVISIONED: 'unprovisioned',

    STATUS_OBJECT: 'sts',

    /*
     * TYPE_* are used with definitions of constraints, and optionally combined
     * with LIST: TYPE_LIST + TYPE_STRING describes a list of strings, and so on.
     */
    TYPE_LIST: 1,   // all other TYPE_* values must be even values!
    TYPE_BOOL: 2,
    TYPE_INT:  4,
    TYPE_FLOAT: 6,
    TYPE_STRING: 8,
    TYPE_OBJECT: 10,

    WEEKDAY_MONDAY: 'mon',
    WEEKDAY_TUESDAY: 'tue',
    WEEKDAY_WEDNESDAY: 'wed',
    WEEKDAY_THURSDAY: 'thu',
    WEEKDAY_FRIDAY: 'fri',
    WEEKDAY_SATURDAY: 'sat',
    WEEKDAY_SUNDAY: 'sun',

    LOG_TABLE: 'GLPLogTable',
    SNAPSHOT_TABLE: 'GLPSnapshotTable',
    ADMIN_TABLE: 'GLPAdminTable'
}

function alt(/* ... */) {
    // Variadic utility, returns regex string of alternatives, e.g. "a|b|c"
    let result = ''
    if (arguments.length) {
        result = arguments[0]
        for (let i = 1; i < arguments.length; ++i) {
            result += '|' + arguments[i]
        }
    }
    return result
}

function group(/* ... */) {
    // Variadic utility, returns a group of alternatives, e.g. "(a|b|c)"
    return '(' + alt(...arguments) + ')'
}

function topic(/* ... */) {
    // Variadic utility, returns a regular expression for a GLP topic string
    // made from its arguments.
    // The '^glp' root is always provided and must not be included with the
    // arguments.
    // **Note** this utility produces a regular expression to match a topic,
    // it does not produce a GLP topic.
    let result = '^' + lit.GLP
    for (let i = 0; i < arguments.length; ++i) {
        result += lit.SEP + arguments[i]
    }
    return result
}

/*
 * A number of regular expressions to match certain topic groups, used with
 * definitions of constraints.
 */
lit.TOPIC_RE_ROOT = topic(   // ^glp/VERSION/SID
    String(lit.GLP_PROTOCOL_VERSION),
    lit.ANY
)

lit.TOPIC_RE_REQUEST = topic(   // ^glp/VERSION/SID/rq
    lit.TOPIC_RE_ROOT,
    lit.REQUEST_CHANNEL
)

lit.TOPIC_RE_FEEDBACK = topic(  // ^glp/VERSION/SID/fb
    lit.TOPIC_RE_ROOT,
    lit.FEEDBACK_CHANNEL
)

lit.TOPIC_RE_ANYCHANNEL = topic(    // ^glp/VERSION/SID/(rq|fb|ev)
    lit.TOPIC_RE_ROOT,
    group(
        lit.REQUEST_CHANNEL,
        lit.FEEDBACK_CHANNEL,
        lit.EVENT_CHANNEL
    )
)

lit.DEVICE_ACTIONS = alt(
    lit.ACTION_CREATE,
    lit.ACTION_DELETE,
    lit.ACTION_DEPROVISION,
    lit.ACTION_LOAD,
    lit.ACTION_PROVISION,
    lit.ACTION_TEST,
    lit.ACTION_REPAIR,
    lit.ACTION_REPLACE,
    lit.ACTION_WINK
)

lit.DEVICE_CATEGORIES = alt(
    lit.DEVICE_CATEGORY_IO,
    lit.DEVICE_CATEGORY_INFRASTRUCTURE,
    lit.DEVICE_CATEGORY_SEGMENT
)

lit.DEVICE_HEALTH_STATES = alt(
    lit.DEVICE_HEALTH_NORMAL,
    lit.DEVICE_HEALTH_SUSPECT,
    lit.DEVICE_HEALTH_DOWN,
    lit.DEVICE_HEALTH_NASCENT
)

lit.DEVICE_STATES = alt(
    lit.STATE_CREATED,
    lit.STATE_DELETED,
    lit.STATUS_STATE_PROVISIONED,
    lit.STATUS_STATE_UNPROVISIONED
)

lit.GROUP_STATES = lit.DEVICE_STATES

lit.JOB_STATES = alt(
    lit.STATE_CREATED,
    lit.STATE_ACTIVE,
    lit.STATE_UPDATED,
    lit.STATE_DELETED
)

lit.SCHEDULE_STATES = alt(
    lit.STATE_CREATED,
    lit.STATE_ACTIVE,
    lit.STATE_INACTIVE,
    lit.STATE_UPDATED,
    lit.STATE_DELETED
)

lit.CONNECTION_STATES = alt(
    lit.STATE_CREATED,
    lit.STATE_DELETED,
    lit.STATUS_STATE_PROVISIONED,
    lit.STATUS_STATE_UNPROVISIONED
)

lit.ALARM_STATES = alt(
    lit.STATE_ENABLED,
    lit.STATE_DISABLED,
    lit.STATE_DELETED
)

let CONNECTION_CHANNEL_RE = topic(
    lit.TOPIC_RE_ROOT,
    '&' + lit.ANY,
    'data$'
)

let CONNECTION_DST_RE = alt(
    CONNECTION_CHANNEL_RE,
    lit.TOPIC_RE_REQUEST
)

let CONNECTION_SRC_RE = alt(
    CONNECTION_CHANNEL_RE,
    lit.TOPIC_RE_FEEDBACK
)

lit.DISCOVERY_METHODS = alt(
    lit.DISCOVERY_METHOD_AUTO,
    lit.DISCOVERY_METHOD_NONE,
    lit.DISCOVERY_METHOD_SATM
)

lit.WEEKDAY_WEEKDAY = alt(
    lit.WEEKDAY_MONDAY,
    lit.WEEKDAY_TUESDAY,
    lit.WEEKDAY_WEDNESDAY,
    lit.WEEKDAY_THURSDAY,
    lit.WEEKDAY_FRIDAY
)

lit.WEEKDAY_WEEKEND = alt(
    lit.WEEKDAY_SATURDAY,
    lit.WEEKDAY_SUNDAY
)

lit.DATAPOINT_DIRECTIONS = alt(
    lit.DATAPOINT_DIRECTION_IN,
    lit.DATAPOINT_DIRECTION_OUT
)

lit.DATAPOINT_MONITORING = alt(
    lit.DATAPOINT_MONITOR_NONE,
    lit.DATAPOINT_MONITOR_SLOW,
    lit.DATAPOINT_MONITOR_NORMAL,
    lit.DATAPOINT_MONITOR_FAST
)

lit.Location_Template = {
    desc: null,
    column: null,
    row: null,
    ele: null,
    lat: null,
    lng: null,
    sunset: -6,
    sunrise: -6
}

lit.Location_Constraints = {
    desc: {
        type: lit.TYPE_STRING
    },
    column : {
        min: 0,
        max: 255,
        type: lit.TYPE_INT
    },
    row: {
        min: 0,
        max: 255,
        type: lit.TYPE_INT
    },
    ele: {
        type: lit.TYPE_FLOAT
    },
    lat: {
        min: -90,
        max: +90,
        type: lit.TYPE_FLOAT
    },
    lng: {
        min: 0,
        max: 360,
        type: lit.TYPE_FLOAT
    },
    sunset: {
        min: -90,
        max: +90,
        type: lit.TYPE_FLOAT
    },
    sunrise: {
        min: -90,
        max: +90,
        type: lit.TYPE_FLOAT
    }
}

lit.Limitations_Template = {
    devices:    null,       // report numbers possible, or unbound
    'devices-per-group': null,
    groups:     null,
    'groups-per-device': null,
    'protocols':    []      // see PROTOCOLS_*
}

lit.About_Template = {
    copyright: null,
    discovery: null,        // see DISCOVERY_METHOD_*
    manufacturer: null,
    model: null,
    product: null,
    version: "0.00.000",    // product version, use x.yy.zzz format
    glpVersion: 0,          // GLP protocol version, use GLP_PROTOCOL_*
    capabilities: [],       // see CAPABILITY_*
    limitations: {}         // See Limitations_*
}

lit.Time_Template = {
    day:    null,
    hour:   null,
    minute: null,
    month:  null,
    second: null,
    tz:     null,
    wday:   null,
    year:   null,
    zone:   null
}

lit.Time_Constraints = {
    day: {
        min: 0,
        max: 31,
        type: lit.TYPE_INT
    },
    hour: {
        min: 0,
        max: 23,
        type: lit.TYPE_INT
    },
    minute: {
        min: 0,
        max: 59,
        type: lit.TYPE_INT
    },
    second: {
        min: 0,
        max: 59,
        type: lit.TYPE_INT
    },
    tz: {
        type: lit.TYPE_STRING
    },
    wday: {
        pattern: alt(
            lit.WEEKDAY_WEEKDAY,
            lit.WEEKDAY_WEEKEND
        ),
        type: lit.TYPE_STRING
    },
    year: {
        min: 0,
        max: 3000,
        type: lit.TYPE_INT
    },
    zone: {
        type: lit.TYPE_STRING
    }
}

lit.InternetServer_Template = {
    address: null,
    port: null,
    protocol: null,
    security: null,
    user: null,
    passwd: null
}

lit.InternetServer_Constraints = {
    address: {
        type: lit.TYPE_STRING
    },
    port: {
        min: 0,
        max: 65535,
        type: lit.TYPE_INT
    },
    protocol: {
        type: lit.TYPE_STRING
    },
    security: {
        type: lit.TYPE_STRING
    },
    user: {
        type: lit.TYPE_STRING
    },
    passwd: {
        type: lit.TYPE_STRING
    }
}

lit.Config_Template = {
    desc: null,
    black: null,
    time:   null,           // See Time_*
    discovery: null,
    events: [],
    key: [],
    loc: null,              // See Location_*
    log_days: null,
    smtp: null,             // See InternetServer_*
    tracing: null,
    uplinkFreq: null,
    uplinkIdle: null,
    uplinkDrop: null,
    white: null
}

lit.Config_Constraints = {
    desc: {
        type: lit.TYPE_STRING
    },
    black: {
        type: lit.TYPE_LIST + lit.TYPE_STRING
    },
    time: {
        type: lit.TYPE_OBJECT,
        pattern: lit.Time_Constraints
    },
    discovery: {
        pattern: lit.DISCOVERY_METHODS,
        type: lit.TYPE_STRING
    },
    events: {
        type: lit.TYPE_LIST + lit.TYPE_STRING
    },
    key: {
        type: lit.TYPE_LIST + lit.TYPE_STRING
    },
    loc: {
        type: lit.TYPE_OBJECT,
        pattern: lit.Location_Constraints
    },
    log_days: {
        min: 0,
        type: lit.TYPE_INT
    },
    smtp: {
        type: lit.TYPE_OBJECT,
        pattern: lit.InternetServer_Constraints
    },
    tracing: {
        type: lit.TYPE_BOOL
    },
    uplinkFreq: {
        min: 0,
        type: lit.TYPE_INT
    },
    uplinkIdle: {
        min: 0,
        type: lit.TYPE_INT
    },
    uplinkDrop: {
        min: 0,
        type: lit.TYPE_INT
    },
    white: {
        type: lit.TYPE_LIST + lit.TYPE_STRING
    }
}

lit.Datapoint_Template = {
    desc: null,
    cat: null,          // DATAPOINT_DIRECTION*
    monitor: null,      // DATAPOINT_MONITOR*
    value: null,
    type: null
}

lit.Datapoint_Constraints = {
    desc: {
        type: lit.TYPE_STRING
    },
    cat: {
        pattern: lit.DATAPOINT_DIRECTIONS,
        type: lit.TYPE_STRING
    },
    monitor: {
        pattern: lit.DATAPOINT_MONITORING,
        type: lit.TYPE_STRING
    },
    // value: not restricted
    type: {
        type: lit.TYPE_STRING
    }
}

lit.Device_Config_Template = {
    name: null,
    desc : null,
    loc: null,
    motion_zone: null,
    motion_radius: null,
    motion_timeout: null
}

lit.Device_Config_Constraints = {
    name: {
        type: lit.TYPE_STRING
    },
    desc: {
        type: lit.TYPE_STRING
    },
    loc: {
        type: lit.TYPE_OBJECT,
        pattern: lit.Location_Constraints
    },
    motion_zone: {
        type: lit.TYPE_INT
    },
    motion_radius: {
        min: 0,
        type: lit.TYPE_INT
    },
    motion_timeout: {
        min: 0,
        type: lit.TYPE_INT
    }
}

lit.Device_Status_Template = {
    action: null,
    addr: null,
    alert: null,
    amd: null,
    cat: null,          // DEVICE_CATEGORIES
    health: null,       // DEVICE_HEALTH_STATES
    manufacturer: null,
    product: null,
    routing: null,
    state: null,        // DEVICE_STATES
    type: null,
    unid: null,
    usage: null,
    version: null
}

lit.Group_Config_Template = {
    desc: null,
    members: []
}

lit.Group_Status_Template = {
    action: null,
    addr: null,
    alert: null,
    state: null         // GROUP_STATES
}

lit.Group_Config_Constraints = {
    desc: {
        type: lit.TYPE_STRING
    },
    members: {
        type: lit.TYPE_LIST + lit.TYPE_STRING
    },   // TODO add pattern
}

lit.Job_Member_Constraints = {
    // obj is not constraint
    tgt: {
        type: lit.TYPE_STRING,
        pattern: lit.TOPIC_RE_REQUEST
    }
}

lit.Job_Config_Template = {
    desc: null,
    members: []
}

lit.Job_Status_Template = {
    state: null                     // use JOB_STATES
}

lit.Job_Config_Constraints = {
    desc: {
        type: lit.TYPE_STRING
    },
    members: {
        type: lit.TYPE_LIST + lit.TYPE_OBJECT,
        pattern: lit.Job_Member_Constraints
    },
}

lit.Schedule_Config_Template = {
    desc: null,
    enabled: false,
    start: null,
    end: null,
    utc: false,
    events: []
}

lit.Schedule_Status_Template = {
    state: null,                    // use SCHEDULE_STATES
}

lit.Schedule_Config_Constraints = {
    desc: {
        type: lit.TYPE_STRING
    },
    enabled: {
        type: lit.TYPE_BOOL
    },
    start: {
        type: lit.TYPE_STRING
    },     // TODO: supply RE
    end: {
        type: lit.TYPE_STRING
    },       // TODO: supply RE
    utc: {
        type: lit.TYPE_BOOL
    },
    events: {
        type: lit.TYPE_LIST + lit.TYPE_STRING,
        pattern: topic(
            lit.TOPIC_RE_FEEDBACK,
            lit.OBJECT_TYPE_SCHEDULED_EVENT,
            lit.ANY,
            '$'
        )
    }
}

lit.ScheduledEvent_Config_Template = {
    desc: null,
    date: null,
    day: null,
    period: 1,
    prio: lit.PRIORITY_DEFAULT,
    utc: false,
    events: []
}

lit.ScheduledEvent_Event_Constraints = {
    time: { type: lit.TYPE_STRING },      // TODO add RE
    job: {
        type: lit.TYPE_STRING,
        pattern: topic(
            lit.TOPIC_RE_REQUEST,
            lit.OBJECT_TYPE_JOB,
            lit.ANY,
            '$'
        )
    }
}

lit.ScheduledEvent_Config_Constraints = {
    desc: {
        type: lit.TYPE_STRING
    },
    date: {
        type: lit.TYPE_STRING
    },      // TODO add RE
    day:  {
        type: lit.TYPE_LIST + lit.TYPE_STRING,
        pattern: alt(
            lit.WEEKDAY_WEEKDAY,
            lit.WEEKDAY_WEEKEND
        )
    },
    period: {
        min: 1,
        type: lit.TYPE_INT
    },
    prio: {
        min: lit.PRIORITY_MIN,
        max: lit.PRIORITY_MAX,
        type: lit.TYPE_INT
    },
    utc: {
        type: lit.TYPE_BOOL
    },
    events: {
        type: lit.TYPE_LIST + lit.TYPE_OBJECT,
        pattern: lit.ScheduledEvent_Event_Constraints
    }
}

lit.ScheduledEvent_Status_Template = {
    state: null,    // SCHEDULEDEVENT_STATES
    'last-activity': null,
    'last-time': null,
    'last-job': null
}

lit.Connection_Config_Template = {
    address: null,
    channel: null,
    dst: null,
    port: null,
    protocol: null,
    src: null
}

lit.Connection_Config_Constraints = {
    address: {
        type: lit.TYPE_STRING
    },
    channel: {
        type: lit.TYPE_STRING
    },
    dst: {
        type: lit.TYPE_LIST + lit.TYPE_STRING,
        pattern: CONNECTION_DST_RE
    },
    port: {
        min: 0,
        max: 65535,
        type: lit.TYPE_INT
    },
    protocol: {
        type: lit.TYPE_STRING
    },
    src: {
        type: lit.TYPE_LIST + lit.TYPE_STRING,
        pattern: CONNECTION_SRC_RE
    }
}

lit.Connection_Status_Template = {
    action: null,
    alert: null,
    implementation: null,
    state: null     // CONNECTION_STATES
}

lit.Alarm_Config_Template = {
    desc: null,
    enabled: true,
    cat: "alarm",
    ackd: true,
    activation: null,
    deactivation: null,
    message: null,
    urgent: false,
    actions: null
}

lit.Alarm_Action_SMS_Constraint = {
    to: {
        type: lit.TYPE_STRING,
        pattern: '[0-9 +]+'
    }
}

lit.Alarm_Action_Email_Constraint = {
    to: {
        min: 3,
        type: lit.TYPE_STRING,
        pattern: '.+@.+'
    },  // TODO: better pattern for valid email
    subject: { min: 1, type: lit.TYPE_STRING }
}

lit.Alarm_Actions_Constraints = {
    sms: {
        type: lit.TYPE_OBJECT,
        pattern: lit.Alarm_Action_SMS_Constraint
    },
    email: {
        type: lit.TYPE_OBJECT,
        pattern: lit.Alarm_Action_Email_Constraint
    }
}

lit.Alarm_Config_Constraints = {
    desc: {
        type: lit.TYPE_STRING
    },
    enabled: {
        type: lit.TYPE_BOOL
    },
    cat: {
        type: lit.TYPE_STRING
    },
    ackd: {
        type: lit.TYPE_BOOL
    },
    activation: {
        type: lit.TYPE_STRING
    },
    deactivation: {
        type: lit.TYPE_STRING
    },
    message: {
        type: lit.TYPE_STRING
    },
    urgent: {
        type: lit.TYPE_BOOL
    },
    actions: {
        type: lit.TYPE_OBJECT,
        pattern: lit.Alarm_Actions_Constraints
    }
}

lit.Alarm_Status_Template = {
    state: null         // ALARM_STATES
}

/*
 * init
 * initializes this module. The function does nothing but is provided for
 * consistency across all toolkit modules.
 */
function init() {

}

/*
 * Last not least, let's export this into the v0 API:
 */
exports.v0 = lit
exports.v0.VERSION = version
exports.v0.init = init
