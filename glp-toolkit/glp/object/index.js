/*
 * Echelon GLP Toolkit for Node.js
 * Copyright (c) 2017 Echelon Corporation
 */
 /* jshint esversion: 6 */
 /* jslint node: true, maxerr: 10000 */
"use strict";
const sqlite3 = require("sqlite3").verbose();
const tools = require("../tools").v0;
let self = require("./package.json");
const version = tools.get_version(self.version);
const lit = require("../spec").v0;
const log = require("../log").v0;
const mqtt = require("../mqttclient").v0;
const dbname = "/tmp/object";
const async = require("async");
let client_name;
let mqtt_subscriber;
let mqtt_extracter;
let config_getter;
let db;
// list of service object
let services = {};
let ii = 0;
let date = new Date();

/*
 * init
 * initializes the module for a client with the given name, yields Error or
 * nothing for success.
 * The function accepts a getter with the signature of config.v0.get,
 * a subscriber with a signature or mqtt.v0.register, and an extracter with a
 * signature of mqtt.v0.extract.
 */
function init(name, getter, subscriber, extracter) {

    let err, err_msg;
    client_name = name;

    if (!getter) {
        err_msg = "Getter function is undefined or null";
        err = new Error(err_msg);
    }
    config_getter = getter;

    if (!err) {
        if (!subscriber) {
            err_msg = "subscriber function is undefined or null";
            err = new Error(err_msg);
        }
    }
    mqtt_subscriber = subscriber;

    if (!err) {
        if (!extracter) {
            err_msg = "extracter function is undefined or null";
            err = new Error(err_msg);
        }
    }
    mqtt_extracter = extracter;

    if(!err) {
      db = create_database(dbname);
    }
    return err;
}

/*
 * register
 * registers an object type, returns Error unless successful.
 *
 * name is the name of the object type, use spec.v0.OBJECT_TYPE_*
 * actions is an instance of the Actions class, see actions() in this modules
 * obj_template, obj_constraints, status_template are template and status
 * objects. Prefer using templates and constraints from spec.v0.*
 */
function register(name, actions, obj_template, obj_constraints, sts_template) {

    let err, err_msg;

    if (!name) {
        err_msg = "name is undefined or null";
        err = new Error(err_msg);
    }

    if (!err) {
        if (!actions) {
            err_msg = "actions is undefined or null";
            err = new Error(err_msg);
        }
    }

    if (!err) {
        if (!obj_template) {
            err_msg = "obj_template is undefined or null";
            err = new Error(err_msg);
        }
    }

    if (!err) {
        if (!obj_constraints) {
            err_msg = "obj_constraints is undefined or null";
            err = new Error(err_msg);
        }
    }

    if (!err) {

        if (!sts_template) {
            err_msg = "sts_template is undefined or null";
            err = new Error(err_msg);
        }
    }

    //service name
    actions.service_name = name;

    let service_obj = {};
    service_obj.action = actions;
    service_obj.obj_template = obj_template;
    service_obj.obj_constraints = obj_constraints;
    service_obj.sts_template = sts_template;
    service_obj.tbl_name = name.replace("/", "_") + "_tbl";
    services[name] = service_obj;

    if (!err) {
        console.log("Creating Tables for service obj: ", service_obj);
        err = create_tbl(service_obj);
    }
    return err;
}

function create_database(dbname) {
    return new sqlite3.Database(dbname, sqlite3.OPEN_CREATE |
                                sqlite3.OPEN_READWRITE);
}

/*
 * create_tbl
 */
function create_tbl(service_obj) {
    let err;
    let query = "CREATE TABLE if not exists ";
    let obj_template_keys = Object.keys(service_obj.obj_template);
    let tbl_params = " (ID INTEGER PRIMARY KEY AUTOINCREMENT, handle INTEGER," +
                     " object TEXT)";
    let sub_query = service_obj.tbl_name + tbl_params;
    query = query + sub_query;
    console.log("Query: ", query);

    db.serialize(function() {
        let stmt = db.prepare(query);
        stmt.run();
        stmt.finalize();
    });
}

/*
 * obj_stringify
 * returns object converted as string
 */
function obj_stringify(obj_template, arg) {

    let query, ret;
    let my_obj = {};

    let arg_keys = Object.keys(arg);
    let obj_keys = Object.keys(obj_template);

    for (let id in obj_keys) {
        if (arg_keys.indexOf(obj_keys[id]) > -1) {
            my_obj[obj_keys[id]] = arg[obj_keys[id]];
        }
    }
    let obj_str = JSON.stringify(my_obj);
    return obj_str;
}

/*
 * is_handle_present
 * check if handle is present in the database
 * returns 1 if
 */
function is_handle_present(tbl_name, handle, callback) {
    let query, ret = 0;
    //callback(ret);
    query = "SELECT EXISTS(SELECT ID from " + tbl_name + " where handle = " +
            handle + ")";
    console.log(query);

    db.each(query, function(err, row) {
        if (!err) {
          Object.keys(row).forEach(function (key) {
              ret = row[key];
          });
          callback(ret);
        } else {
            console.log(err);
            log.error(err);
        }
    });
}


/*
 * insert_obj_into_db:
 * This function inserts an object in the database
 */
function insert_obj_into_db(tbl_name, obj_str, handle, callback) {
    console.log("======INSERT=======");
    let query, ret;
    let my_obj = {};
    obj_str = "'" + obj_str + "'";
    query = "INSERT INTO " + tbl_name +
            " VALUES(" + "null" + "," + handle + "," + obj_str + ")";
    console.log(query);
    db.run(query, function(err, row) {
        if (err) {
            callback(err);
            console.log(err);
            console.log("Error inserting data");
        } else {
            //TODO : Check if calling back null is the right way
            callback(null);
        }
    });
}

/*
 * update_obj_in_db:
 * This function updates the object in the database
 */
function update_obj_in_db(tbl_name, obj_str, handle, callback){
    console.log("======UPDATE======");
    let query, ret;
    let my_obj = {};
    obj_str = "'" + obj_str + "'";
    query = "UPDATE " + tbl_name +
            " SET object = " + obj_str +
            " where handle = " + handle;
    console.log(query);
    db.run(query, function(err, row) {
        if (err) {
            console.log(err);
            console.log("Error updating data");
            callback(err);
        } else {
            callback(null);
        }
    });

}

function delete_obj_from_db(tbl_name, obj_str, handle, callback) {
    console.log("======DELETE======");
    let query, ret;
    let my_obj = {};
    obj_str = "'" + obj_str + "'";
    query = "DELETE FROM " + tbl_name +
            " where handle = " + handle;
    console.log(db);
    console.log(query);
    db.run(query, function(err, row) {
        if (err) {
            console.log(err);
            console.log("Error deleting data");
            callback(err);
        } else {
            callback(null);
        }
    });

}
/*
 * publish_cfg_object:
 * This function publishes the config object on the feedback channel.
 */
function publish_cfg_object(topic, cfg_obj, callback) {
    topic = mqtt.transform(topic, lit.FEEDBACK_CHANNEL, 'cfg', 1);
    mqtt.publish(topic, cfg_obj, false, 1, function(err, topic, msg) {
        if(!err) {
            let message = "Published cfg object:" + topic + msg;
            log.info("Published cfg object", topic, msg);
            callback(null, message);
        } else {
            callback(err, null);
        }
    });
}

/*
 * publish_sts_object:
 * This function publishes a status object on the feedback channel
 */

function publish_sts_object(topic, state, callback) {
    topic = mqtt.transform(topic, lit.FEEDBACK_CHANNEL, 'sts', 1);
    lit.Job_Status_Template.state = state;
    let sts_obj = JSON.stringify(lit.Job_Status_Template);
    mqtt.publish(topic, sts_obj, false, 1, function(err, topic, msg) {
        if(!err) {
            let message = "Published sts object:" + topic + msg;
            log.info("Published sts object", topic, msg);
            callback(null, message);
        } else {
            callback(err, null);
        }
    });
}

/*
 * Action
 * Object for validator, extracter and completer methode for each action
 * like "create" will have object type Action
 */
function Action(service_name, validator, extracter, completer) {
    this.service_name = service_name;

    if (validator) {
        this.validate = validator;
    }

    if (extracter) {
        this.execute = extracter;
    }

    if (completer) {
        this.complete = completer;
    }
}


Action.prototype = {

    validate: function(name, err, topic, handle, action, args) {
        let obj_str;
        let obj_template, tbl_name;
        let err_msg;

        switch(action) {
            case lit.ACTION_CREATE:
                if(err) {
                    log.error(err);
                }
            break;

            case lit.ACTION_UPDATE:
                /*obj_template = services[name].obj_template;
                obj_str = obj_stringify(obj_template, args);
                console.log("OBJ_STR=>",obj_str);

                tbl_name = services[name].tbl_name;
                if (!find_obj(tbl_name, obj_str)) {
                    err_msg = "Object not found";
                    err = new Error(err_msg);
                }*/
            break;

            case lit.ACTION_DELETE:
            break;
        }
        return err;
    },

    execute: function(name, topic, handle, action, args) {
        let obj_str, error, err_msg;
        let obj_template, tbl_name;

        switch (action) {
            // TODO: check if handle exists with a different object
            case lit.ACTION_CREATE:
                // if obj is null or undefined, it means its not present in
                // the database
                obj_template = services[name].obj_template;
                if(args) {
                    obj_str = obj_stringify(obj_template, args);
                }

                tbl_name = services[name].tbl_name;
                // Check if the handle is already present in the table
                is_handle_present(tbl_name, handle, function(ret){
                    // If handle is present, get the object
                    if(ret === 1) {
                      get_obj(tbl_name, handle, obj_str, function(err, res) {
                          // If the object in the database is different from the
                          // object to be created raise an error
                          if(res !== obj_str) {
                              err_msg = "Handle already exists with a "+
                                        "different  object:" + res;
                              err = new Error(err_msg);
                          } else {
                              // Publish duplicate create cfg and sts on fb
                              // channel
                              console.log("Coming here duplicate");
                              publish_cfg_object(topic, obj_str, function(err, res){
                                  if(err) {
                                      error = err;
                                  }
                              });

                              if(!error) {
                                  publish_sts_object(topic, lit.STATE_CREATED,
                                                    function(err, res){
                                                        if(err) {
                                                            error = err;
                                                        } else{
                                                            console.log(res);
                                                        }
                                  });
                              }
                          }
                      });

                    } else {
                          insert_obj_into_db(tbl_name, obj_str, handle, function(err){
                              if(err){
                                error = err;
                              }
                          });
                          if(!error) {
                              publish_cfg_object(topic, obj_str, function(err, res){
                                  if(err) {
                                      error = err;
                                  } else {
                                      console.log(res);
                                  }
                              });
                          }

                          if(!error) {
                              publish_sts_object(topic, lit.STATE_CREATED, function(err, res) {
                                  if(err) {
                                      error = err;
                                  } else {
                                      console.log(res);
                                  }
                              });
                          }
                    }
                });

            break;

            case lit.ACTION_UPDATE:
            // TODO: Report completion with info diagnostics
                    obj_template = services[name].obj_template;
                    if(args) {
                        obj_str = obj_stringify(obj_template, args);
                    }
                    tbl_name = services[name].tbl_name;

                    is_handle_present(tbl_name, handle, function(ret){
                        if(ret === 1){
                            get_obj(tbl_name, handle, obj_str, function(err, res){
                                if(!err) {
                                  update_obj_in_db(tbl_name, obj_str, handle, function(err) {
                                      if(err) {
                                          error = err;
                                      }
                                  });
                                  if(!error) {
                                      publish_cfg_object(topic, obj_str, function(err, res) {
                                          if(err) {
                                              error = err;
                                          } else {
                                              console.log(res);
                                          }
                                      });
                                  }
                                  if(!error) {

                                      publish_sts_object(topic, lit.STATUS_STATE_UPDATED, function(err, res) {
                                          if(err) {
                                              error = err;
                                          } else {
                                              console.log(res);
                                          }
                                      });
                                  }
                                }
                            });
                        } else {
                            log.error("Object not found");
                        }
                    });

            break;

            case lit.ACTION_DELETE:
                  obj_template = services[name].obj_template;
                  if(args) {
                      obj_str = obj_stringify(obj_template, args);
                  }
                  tbl_name = services[name].tbl_name;
                  is_handle_present(tbl_name, handle, function(ret) {
                      if(ret === 1) {
                          get_obj(tbl_name, handle, obj_str, function(err, res) {
                              delete_obj_from_db(tbl_name, obj_str, handle, function(err){
                                  if(err) {
                                      error = err;
                                  }
                              });
                              if(!error) {
                                  publish_cfg_object(topic, null, function(err, res) {
                                      if(err) {
                                          error = err;
                                      } else {
                                          console.log(res);
                                      }
                                  });
                              }
                              if(!error) {
                                  publish_sts_object(topic, lit.STATE_DELETED, function(err, res) {
                                      if(err) {
                                          error = err;
                                      } else {
                                          console.log(res);
                                      }
                                  });
                              }
                          });
                      } else {
                         log.error("Object not found");
                     }
                  });

            break;
       }
    },

    complete: function(err, topic, handle, action, args) {
       switch (action) {
            case lit.ACTION_CREATE:
              if(err) {
                  log.error(err);
              } else {
                  let info_msg = "Created object for topic:" + topic +
                                 "with handle:" + handle + "args:" + args;
                  log.info(info_msg);
              }
            break;

            case lit.ACTION_UPDATE:
            break;

            case lit.ACTION_DELETE:
            break;
       }
    }
};


/*
 * here operation is a dictionary which has supported actions
 * for each action we will have
 */
function Actions(service_name) {
    this.service_name = service_name;
}


Actions.prototype = {
    /*
     * register
     * an action by name, supplying optional validator, extracter and completer
     * callbacks with the following signatures:
     * validator(error, topic, handle, action, args, item) => Error | undefined
     * extracter(topic, handle, action, args, item) => Error | Object
     * completor(error, topic, handle, action, args, item) => undefined
     */
    register: function(name, validator, extracter, completer) {
        this[name] = new Action(this.service_name, validator, extracter,
                                completer);
    }
};

/*
 * actions
 * provides a new and empty Actions object.
 */
function actions(service_name) {
    return new Actions(service_name);
}


function Constraints(template) {
    // TODO implement

}

Constraints.prototype = {
    /*
     * register
     * a new property with a constraints set. A constraints set is an object
     * with zero or more of these properties: min, max, pattern, type, validator.
     */
    register: function(name, constraints) {
        // TODO implement
    }
};

/*
 * constraints
 * returns a new and empty Constraints object
 */
function constraints(template) {
    if(template) {
        return new Constraints();
    } else {
        return new Constraints(template);
    }

}

/*
 * get
 * is a generator, returning an iterator to iterate over all applicable items.
 * Topic can be an absolute topic or one that uses MQTT wildcards.
 */
function get(topic) {
    // TODO implement generator using yield
}

function Storage() {
    // TODO see if storage for this client already exists, read from persistent
    //      storage if this is the case.
    // TODO implement remainder of constructor
}

Storage.prototype = {
    /*
     * register
     * an item of typeof "" or typeof 1 with a name and an initial value.
     * Nothing happens if an item of this name and type is already registered.
     */
    register: function(name, type, initial) {
        // TODO implement
    }
};

/*
 * storage
 * return the client's Storage object. This may not be empty if previously
 * defined.
 */
function storage() {
    return new Storage();
}

/*
 * find_obj()
 */
function find_obj(tbl_name, obj_str) {
    let query, ret;
    let my_obj = {};

    query = "SELECT ID from " + tbl_name + " where object=" + obj_str;
    console.log(query);

    db.each(query, function(err, row) {
        if (!err) {
            console.log(row);
            ret = 1;
        } else {
            ret = 0;
        }
    });
    console.log(ret);
    db.close();
    return ret;
}

/*
 * parse
 * return handle, topic, arguments, item and action from mqtt messages
 */
function parse(topic, msg, name, callback) {

    let err, err_msg;
    let mytopic, handle, action, arg, item;

    if (!msg.action) {
        err_msg = "Action can be null or undefined";
        err = new Error(err_msg);
    }
    action = msg.action;

    if (!err) {
        if(!msg.args) {
            err_msg = "Action can be null or undefined";
            err = new Error(err_msg);
        }
    }
    arg = msg.args;
    // remove 'do' from the topic
    mytopic = topic.replace(/\/([^\/]*)$/,'');

    // extract handle from the topic
    let regex = '[^\/]*$';
    handle = mytopic.match(regex)[0];
    callback([handle, arg, action, topic]);
}


/*
 * get_obj
 */
function get_obj(tbl_name, handle, obj_str, callback) {
    //FIXME: dbname needs to be passed from config file

    let query, ret, val, value;
    let my_obj = {};
    let str = "'" + obj_str + "'";
    query = "SELECT EXISTS(SELECT object from " + tbl_name +
            " where handle = " + handle + ")";
    console.log("Query =>", query);
    db.each(query, function(err, row) {
        if (err) {
            callback(err, null);
        } else {
              Object.keys(row).forEach(function (key) {
                  val = row[key];
              });
              if(val === 1) {
                  db.all("SELECT object from " + tbl_name +
                         " where handle = " + handle , function(err, row){
                      if (row[0].object) {
                          ret = JSON.parse(row[0].object);
                          callback(null, ret);
                      }
                  });
              } else {
                    callback("No records found", null);
              }
        }
    });
}

/*
 * approve:
 * determines whether an assignment of a new value to a property is acceptable
 * under the data conversion rules and constraints
 */
function approve(current_value, proposed_value, constraints) {
    let err_msg;
    let curr_type = typeof(current_value);
    let prop_type = typeof(proposed_value);

    // If type compatible by default
    if(curr_type === prop_type) {
        constraints[current_value] = proposed_value;
    }
    // If current_value type is a string and proposed_value type is a number
    else if(curr_type === typeof("") && prop_type === typeof(1)) {
          constraints[current_value] = proposed_value.toString();
    }
    // If current_value type is a Boolean and proposed_value type is a number
    else if(curr_type === typeof(true) && prop_type === typeof(1)) {
          if(proposed_value === 0) {
              constraints[current_value] = false;
          } else {
              constraints[current_value] = true;
          }
    }
    // If current_value type is a number and proposed_value type is a string
    else if(curr_type === typeof(1) && prop_type === typeof("")) {
          constraints[current_value] = Number(proposed_value);
    }
    // If current_value type is a Boolean and proposed_value type is a string
    else if(curr_type === typeof(true) && prop_type === typeof("")) {
          if(proposed_value === "0" || proposed_value === "off" ||
             proposed_value === "false") {
                constraints[current_value] = false;
          } else if(proposed_value === "1" || proposed_value === "on" ||
                    proposed_value === "true"){
                constraints[current_value] = true;
          } else {
              err_msg = "Type conversion from String to Boolean failed for:" +
                        proposed_value;
              return new Error(err_msg);
          }
    }
    // If current_value type is a number and proposed_value type is a Boolean
    else if(curr_type === typeof(1) && prop_type === typeof(true)) {
          if(proposed_value === false) {
              constraints[current_value] = 0;
          } else if(proposed_value === true) {
              constraints[current_value] = 1;
          }
    }
    // If current_value type is a string and proposed_value type is a Boolean
    else if(curr_type === typeof("") && prop_type === typeof(true)) {
        if(proposed_value === "false") {
            constraints[current_value] = false;
        } else if(proposed_value === "true") {
            constraints[current_value] = true;
        }
    } else {
        err_msg = "Type compatibility failed for constraint with current value"+
                  " of type " + curr_type + " and proposed value of type " +
                  prop_type;
        return new Error(err_msg);
    }
}

exports.v0 = {
    VERSION: version,
    init: init,
    actions: actions,
    constraints: constraints,
    get: get,
    register: register,
    storage: storage,
    parse: parse,
    approve: approve
};
